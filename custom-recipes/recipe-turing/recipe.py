# -*- coding: utf-8 -*-
import dataiku
from dataiku.customrecipe import *
import pandas as pd, numpy as np
from dataiku import pandasutils as pdu
import turing as tm

# Retrieve the name of the datasets
input_dataset_name = get_input_names_for_role("input")[0]
instructions_dataset_name = get_input_names_for_role("instructions")[0]
output_dataset_name = get_output_names_for_role("output")[0]

# Read recipe inputs
input_df = dataiku.Dataset(input_dataset_name).get_dataframe()
input_values = input_df["values"].values.tolist()
input_values = [x if x != "X" else None for x in input_values]

instructions_df = dataiku.Dataset(instructions_dataset_name).get_dataframe()
instructions = instructions_df.values.tolist()

# Init the Turing machine
nb_states = len(pd.unique(instructions_df["State"]))
machine = tm.Machine(nb_states)

# Clean the instructions
for instruction in instructions:
    # current state
    instruction[0] = int(instruction[0])
    # read
    if instruction[1] == "X":
        instruction[1] = None
    else:
        instruction[1] = int(instruction[1])
    # write
    if instruction[2] == "X":
        instruction[2] = None
    else:
        instruction[2] = int(instruction[2])
    # move
    if instruction[3] == "R":
        instruction[3] = tm.Move.RIGHT
    if instruction[3] == "L":
        instruction[3] = tm.Move.LEFT
    if instruction[3] == "X":
        instruction[3] = tm.Move.ANY
    # next state
    if instruction[4] == "F":
        instruction[4] = tm.END
    else:
        instruction[4] = int(instruction[4])

# Feed the instructions to the Turing machine
machine.instruct(instructions)

# Run the Turing machine
result = machine.run(input_values)

# Write recipe outputs
output_df = pd.DataFrame(result, columns=["values"]).astype("int32")
output_dataset = dataiku.Dataset(output_dataset_name)
output_dataset.write_with_schema(output_df)
