# [Real world challenge] Solve ANY computable problem in DSS?

**Disclaimer**: This article is meant to be taken as a second degree joke.

## What's the problem?

*"I manage a DSS instance for my organization, but since I cannot trust my colleagues, I decided to prevent them from writing code in DSS. Obviously, they are now very angry with me because they can't do their very advanced computations anymore. What can I do to let them create algorithms in DSS without letting them write some code?"*

## What's the solution?

Hopefully, this problem has a very obvious solution: using a plugin recipe that simulates an entire Turing machine in DSS.

This solution is **practical**, **useful**, completely **safe** and lightning **fast**.

### Example: Practical use case

Let's create a program that adds 1 to the input.

So, the equivalent of:

```python
def add_1(x):
    return x + 1
```

### 1. Create a dataset with the set of instructions for your Turing machine

Let's create a dataset that will contain the instructions for the Turing machine.

Here, our machine will have 4 different states (0, 1, 2 and F).

![Instructions](images/instructions.png)

**How to read these instructions?**

For instance, the sixth line states that when the machine is in state 2 and reads a "1", it should replace the "1" with a "0", then move to the bit on the left and remain in state 2.

For more explanation, have a look at this [link](https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/turing-machine/one.html) or search "Turing Machine" with your web browser.

**Note that:**

- The first state should be 0.
- The final state should be "F".
- When the machine reads nothing, set the read value to "X".
- When you don't want to move or write anything, set the write value to "X".
- "R" means "move right", "L" means "move left".

### 2. Create an input dataset

Let's try our set of instructions with the input "9". The binary representation of "9" is "1001". So we create a dataset with only column called "values" that will contain 4 integers: "1", "0", "0" and "1".

![Input](images/input.png)

### 3. Run the recipe

Select the recipe "Turing Machine".

![Select recipe](images/select-recipe.png)

![Recipe](images/recipe.png)

Select the datasets we just created as input, and create a new output dataset.

![Select datasets](images/select-datasets.png)

The flow should now contain the Turing Machine recipe:

![Flow](images/flow.png)

### 4. Profit

Tada!!! We obtain "1010" which is the binary for "10". So everything worked as expected.

![Result](images/result.png)

## How does it scale?

Now, what's **amazing** with this solution is that we can chain algorithms. For instance, the following Flow is a very elegant way to compute the discriminant of the quadratic polynomial (b² - 4ac):

![Discriminant](images/discriminant.png)

## Where can I find the code for the plugin?

https://gitlab.com/g-dv/dss-plugin-turing

## Are there some bugs in the plugin implementation?

Certainly!

## Can I find more examples of valid instructions?

Of course, here are a few:

- **Multiply by 2**

| State | Read | Write | Move | Next state |
|-------|------|-------|------|------------|
| 0     | X    | X     | R    | 1          |
| 1     | X    | 0     | X    | F          |
| 1     | 0    | 0     | R    | 1          |
| 1     | 1    | 1     | R    | 1          |

- **Add 1**

| State | Read | Write | Move | Next state |
|-------|------|-------|------|------------|
| 0     | X    | X     | R    | 1          |
| 1     | 0    | 0     | R    | 1          |
| 1     | 1    | 1     | R    | 1          |
| 1     | X    | X     | L    | 2          |
| 2     | 0    | 1     | X    | F          |
| 2     | 1    | 0     | L    | 2          |
| 2     | X    | 1     | X    | F          |

- **Substract 1**

| State | Read | Write | Move | Next state |
|-------|------|-------|------|------------|
| 0     | X    | X     | R    | 1          |
| 1     | 0    | 0     | R    | 1          |
| 1     | 1    | 1     | R    | 1          |
| 1     | X    | X     | L    | 2          |
| 2     | 0    | 1     | L    | 2          |
| 2     | 1    | 0     | X    | F          |
| 2     | X    | X     | X    | F          |

- **Double a list of 1s**

| State | Read | Write | Move | Next state |
|-------|------|-------|------|------------|
| 0     | X    | X     | R    | 1          |
| 1     | 0    | 0     | R    | 1          |
| 1     | 1    | 0     | L    | 2          |
| 1     | X    | X     | L    | 3          |
| 2     | 0    | 0     | L    | 2          |
| 2     | X    | 0     | R    | 1          |
| 3     | 0    | 1     | L    | 3          |
| 3     | X    | X     | L    | F          |
