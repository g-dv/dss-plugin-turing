from enum import Enum
from collections import deque


START = 0
END = -1


class Move(Enum):
    LEFT = -1
    RIGHT = 1
    ANY = 0


class Machine:
    """
    The Turing machine framework
    Must be taught some instructions with the ìnstruct` method before
    being able to execute them on some inputs with the `run` method
    """
    class Memory:
        """
        The memory tape of the Turing machine
        Implemented as a double linked list
        """
        class Node:
            """
            One node of the memory tape (double-linked list)
            """
            def __init__(self, data, prev, next):
                self.data = data
                self.prev = prev
                self.next = next

        def __init__(self, data):
            self.nodes = deque([self.Node(None, None, None)])
            for e in data:
                self.nodes.append(self.Node(e, self.nodes[-1], None))
                self.nodes[-2].next = self.nodes[-1]
            self.current = self.nodes[0]

        def read(self):
            """
            Read the active memory position
            """
            return self.current.data

        def write(self, data):
            """
            Replace the value at the active memory position
            """
            self.current.data = data

        def move(self, direction):
            """
            Move the cursor of the memory tape to the right or to the left
            """
            if direction == Move.RIGHT:
                if self.current.next == None:
                    self.nodes.append(self.Node(None, self.current, None))
                    self.current.next = self.nodes[-1]
                self.current = self.current.next
            elif direction == Move.LEFT:
                if self.current.prev == None:
                    self.nodes.appendleft(self.Node(None, None, self.current))
                    self.current.prev = self.nodes[0]
                self.current = self.current.prev

    def __init__(self, nb_states):
        self.instructions = [{None: [], 0: [], 1: []} for _ in range(nb_states)]

    def run(self, data):
        """
        Execute the instructions of the machine on the given parameter
        """
        state = START
        memory = self.Memory(data)
        while state != END:
            write, move, state = self.instructions[state][memory.read()]
            memory.write(write)
            memory.move(move)
        return [node.data for node in memory.nodes if node.data != None]

    def instruct(self, instructions):
        """
        Adds instructions for the Turing machine
        """
        for state, read, write, move, next_state in instructions:
            self.instructions[state][read] = [write, move, next_state]
